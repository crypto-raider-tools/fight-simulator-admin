FROM node:16-alpine

WORKDIR /srv

RUN yarn global add serve

COPY ./yarn.lock    /srv/yarn.lock 
COPY ./package.json /srv/package.json 

RUN yarn install --frozen-lockfile

COPY ./public       /srv/public 
COPY ./src          /srv/src

RUN yarn build

CMD ["serve", "-s", "/srv/build"]
EXPOSE 3000
