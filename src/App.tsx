import { Redirect, Route } from "react-router-dom";
import {
  hydraDataProvider as baseHydraDataProvider,
  fetchHydra as baseFetchHydra,
  useIntrospection,
  CreateGuesser,
  InputGuesser,
  EditGuesser,
  ApiPlatformAdminDataProvider,
  ApiDocumentationParserResponse,
  HydraAdmin,
} from "@api-platform/admin";
import { parseHydraDocumentation } from "@api-platform/api-doc-parser";
import authProvider from "./authProvider";
import { FieldGuesser, ListGuesser, ResourceGuesser } from "@api-platform/admin";
import { ENTRYPOINT } from "./config";
import { createBrowserHistory } from "history";
import {
  AutocompleteArrayInput,
  ChipField,
  ReferenceArrayField,
  ReferenceArrayInput,
  SelectArrayInput,
  SingleFieldList,
} from "react-admin";
import { useEffect, useState } from "react";

const getStoredAuth = () => {
  try {
    return JSON.parse(localStorage.getItem("auth"));
  } catch (error) {
    return {};
  }
};

const getHeaders = () =>
  getStoredAuth()?.token
    ? {
        Authorization: `Bearer ${getStoredAuth()?.token}`,
      }
    : {};
const fetchHydra = (url, options = {}) =>
  baseFetchHydra(url, {
    ...options,
    headers: getHeaders,
  });
const RedirectToLogin = () => {
  const introspect = useIntrospection();

  if (getStoredAuth()?.token) {
    introspect();
    return <></>;
  }
  return <Redirect to="/login" />;
};
const apiDocumentationParser = async (): Promise<ApiDocumentationParserResponse> => {
  try {
    return await parseHydraDocumentation(ENTRYPOINT, { headers: getHeaders });
  } catch (result) {
    if (result.status !== 401) {
      throw result;
    }

    localStorage.removeItem("auth");

    return Promise.resolve({
      ...result,
      customRoutes: [<Route key="/" path="/" component={RedirectToLogin} />],
    });
  }
};

const dataProvider: ApiPlatformAdminDataProvider = baseHydraDataProvider({
  entrypoint: ENTRYPOINT,
  httpClient: fetchHydra,
  apiDocumentationParser,
});

const AdminLoader = () => {
  const [history, setHistory] = useState(createBrowserHistory());

  useEffect(() => {
    let historyOption = {};
    if (window.location.host === "crypto-raider-tools.gitlab.io") {
      historyOption = { basename: "fight-simulator-admin" };
    }
    setHistory(createBrowserHistory(historyOption));
  }, []);

  return (
    <HydraAdmin history={history} dataProvider={dataProvider} authProvider={authProvider} entrypoint={ENTRYPOINT}>
      <ResourceGuesser
        name={"dungeons"}
        list={(props) => (
          <ListGuesser {...props}>
            <FieldGuesser source={"name"} />
            <FieldGuesser source={"difficulty"} />
            <FieldGuesser source={"act"} />
            <FieldGuesser source={"keyCost"} />
            <FieldGuesser source={"recommendedTotalStatPoints"} />
            <FieldGuesser source={"winXp"} />
            <ReferenceArrayField source="mobs" reference="mobs">
              <SingleFieldList>
                <ChipField source="name" />
              </SingleFieldList>
            </ReferenceArrayField>
          </ListGuesser>
        )}
        edit={(props) => (
          <EditGuesser {...props}>
            <InputGuesser source={"name"} />
            <InputGuesser source={"cliName"} />
            <InputGuesser source={"act"} />
            <InputGuesser source={"keyCost"} />
            <InputGuesser source={"recommendedTotalStatPoints"} />
            <InputGuesser source={"levelMin"} />
            <InputGuesser source={"levelMax"} />
            <InputGuesser source={"internalName"} />
            <InputGuesser source={"winXp"} />
            <InputGuesser source={"loseXp"} />
            <ReferenceArrayInput source={"droppableItems"} reference={"stuff_stats"}>
              <AutocompleteArrayInput optionText="name" />
            </ReferenceArrayInput>
            <InputGuesser source={"iconPosX"} />
            <InputGuesser source={"iconPosY"} />
            <InputGuesser source={"iconUrl"} />
            <InputGuesser source={"difficulty"} />
            <ReferenceArrayInput source={"mobs"} reference={"mobs"}>
              <AutocompleteArrayInput optionText="name" />
            </ReferenceArrayInput>
          </EditGuesser>
        )}
      />
      <ResourceGuesser
        name={"mobs"}
        list={(props) => (
          <ListGuesser {...props}>
            <FieldGuesser source={"name"} />
            <ReferenceArrayField source="dungeons" reference="dungeons">
              <SingleFieldList>
                <ChipField source="name" />
              </SingleFieldList>
            </ReferenceArrayField>
          </ListGuesser>
        )}
        create={(props) => (
          <CreateGuesser {...props}>
            <InputGuesser source={"name"} />
            <InputGuesser source={"type"} />
            <InputGuesser source={"level"} />
            <InputGuesser source={"effectiveAgi"} />
            <InputGuesser source={"effectiveChr"} />
            <InputGuesser source={"effectiveInt"} />
            <InputGuesser source={"effectiveLuk"} />
            <InputGuesser source={"effectiveStr"} />
            <InputGuesser source={"effectiveWis"} />
            <InputGuesser source={"critDamageMultiplier"} />
            <InputGuesser source={"critResist"} />
            <InputGuesser source={"evadeChance"} />
            <InputGuesser source={"extraHP"} />
            <InputGuesser source={"hitChance"} />
            <InputGuesser source={"hitFirst"} />
            <InputGuesser source={"maxDamage"} />
            <InputGuesser source={"maxHP"} />
            <InputGuesser source={"meleeCrit"} />
            <InputGuesser source={"meleeResist"} />
            <InputGuesser source={"minDamage"} />
            <ReferenceArrayInput source={"dungeons"} reference={"dungeons"}>
              <AutocompleteArrayInput optionText="name" />
            </ReferenceArrayInput>
            <InputGuesser source={"idleUrl"} />
            <InputGuesser source={"idleDimensionX"} />
            <InputGuesser source={"idleDimensionY"} />
            <InputGuesser source={"idleAnimationSteps"} />
          </CreateGuesser>
        )}
        edit={(props) => (
          <EditGuesser {...props}>
            <InputGuesser source={"name"} />
            <InputGuesser source={"type"} />
            <InputGuesser source={"level"} />
            <InputGuesser source={"effectiveAgi"} />
            <InputGuesser source={"effectiveChr"} />
            <InputGuesser source={"effectiveInt"} />
            <InputGuesser source={"effectiveLuk"} />
            <InputGuesser source={"effectiveStr"} />
            <InputGuesser source={"effectiveWis"} />
            <InputGuesser source={"critDamageMultiplier"} />
            <InputGuesser source={"critResist"} />
            <InputGuesser source={"evadeChance"} />
            <InputGuesser source={"extraHP"} />
            <InputGuesser source={"hitChance"} />
            <InputGuesser source={"hitFirst"} />
            <InputGuesser source={"maxDamage"} />
            <InputGuesser source={"maxHP"} />
            <InputGuesser source={"meleeCrit"} />
            <InputGuesser source={"meleeResist"} />
            <InputGuesser source={"minDamage"} />
            <ReferenceArrayInput source={"dungeons"} reference={"dungeons"}>
              <AutocompleteArrayInput optionText="name" />
            </ReferenceArrayInput>
            <InputGuesser source={"idleUrl"} />
            <InputGuesser source={"idleDimensionX"} />
            <InputGuesser source={"idleDimensionY"} />
            <InputGuesser source={"idleAnimationSteps"} />
          </EditGuesser>
        )}
      />
      <ResourceGuesser
        name={"stuff_stats"}
        list={(props) => (
          <ListGuesser {...props}>
            <FieldGuesser source={"name"} />
            <FieldGuesser source={"count"} />
            <FieldGuesser source={"internalName"} />
            <FieldGuesser source={"slot"} />
            <FieldGuesser source={"gearType"} />
            <FieldGuesser source={"rarity"} />
            <FieldGuesser source={"pointQtyMax"} />
            <FieldGuesser source={"pointQtyMin"} />
          </ListGuesser>
        )}
        edit={(props) => (
          <EditGuesser {...props}>
            <InputGuesser source={"identifier"} />
            <InputGuesser source={"name"} />
            <InputGuesser source={"slot"} />
            <InputGuesser source={"count"} />
            <InputGuesser source={"strengthMax"} />
            <InputGuesser source={"strengthMin"} />
            <InputGuesser source={"intelligenceMax"} />
            <InputGuesser source={"intelligenceMin"} />
            <InputGuesser source={"agilityMax"} />
            <InputGuesser source={"agilityMin"} />
            <InputGuesser source={"wisdomMax"} />
            <InputGuesser source={"wisdomMin"} />
            <InputGuesser source={"charmMax"} />
            <InputGuesser source={"charmMin"} />
            <InputGuesser source={"luckMax"} />
            <InputGuesser source={"luckMin"} />
            <InputGuesser source={"internalName"} />
            <InputGuesser source={"gearType"} />
            <InputGuesser source={"rarity"} />
            <InputGuesser source={"levelMax"} />
            <InputGuesser source={"pointQtyMax"} />
            <InputGuesser source={"pointQtyMin"} />
            <ReferenceArrayInput source={"dungeons"} reference={"dungeons"}>
              <AutocompleteArrayInput optionText="name" />
            </ReferenceArrayInput>
          </EditGuesser>
        )}
      />
      <ResourceGuesser name={"sync_stuffs"} />
      <ResourceGuesser name={"raider_abilities"} />
      <ResourceGuesser
        name={"sync_raiders"}
        list={(props) => (
          <ListGuesser {...props}>
            <FieldGuesser source={"currentlyInWallet"} />
            <FieldGuesser source={"name"} />
            <FieldGuesser source={"level"} />
            <FieldGuesser source={"race"} />
            <FieldGuesser source={"generation"} />
            <FieldGuesser source={"raidsCompleted"} />
            <FieldGuesser source={"agility"} />
            <FieldGuesser source={"strength"} />
            <FieldGuesser source={"charm"} />
            <FieldGuesser source={"intelligence"} />
            <FieldGuesser source={"luck"} />
            <FieldGuesser source={"wisdom"} />
            <FieldGuesser source={"updatedAt"} />
          </ListGuesser>
        )}
      />
      <ResourceGuesser name={"configs"} />
      <ResourceGuesser
        name={"users"}
        edit={(props) => (
          <EditGuesser {...props}>
            <InputGuesser source={"username"} />
            <InputGuesser source={"password"} />
            <SelectArrayInput
              label="Roles"
              source={"roles"}
              choices={[
                { id: "ROLE_USER", name: "User" },
                { id: "ROLE_SUPER_ADMIN", name: "Super admin" },
              ]}
            />
          </EditGuesser>
        )}
        create={(props) => (
          <CreateGuesser {...props}>
            <InputGuesser source={"username"} />
            <InputGuesser source={"password"} />
            <SelectArrayInput
              label="Roles"
              source={"roles"}
              choices={[
                { id: "ROLE_USER", name: "User" },
                { id: "ROLE_SUPER_ADMIN", name: "Super admin" },
              ]}
            />
          </CreateGuesser>
        )}
      />
      <ResourceGuesser
        name={"customers"}
        list={(props) => (
          <ListGuesser {...props}>
            <FieldGuesser source={"metamaskAddress"} />
            <FieldGuesser source={"isDonator"} />
            <FieldGuesser source={"isPremium"} />
          </ListGuesser>
        )}
        edit={(props) => (
          <EditGuesser {...props}>
            <InputGuesser source={"premiumExpirationDate"} />
          </EditGuesser>
        )}
      />
    </HydraAdmin>
  );
};

const Admin = () => (
  <>
    <title>CRC API Admin</title>

    <AdminLoader />
  </>
);
export default Admin;
